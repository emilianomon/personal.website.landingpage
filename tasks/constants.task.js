'use strict';

module.exports = (gulp, ngConfig, fs, concat, babel, uglify) => {
    return () => {
        return gulp.src([
            './src/app/constants/init.constant.json',
        ])
        .pipe(ngConfig('portfolio.angular', {
            createModule: false,
            wrap: true,
            pretty: true,
            constants: {
                "ENV": getEnvConstants(fs, 'dev'), // Get value from specific param or procedure.
                "LOCALE": getLocaleConstants(fs)
            }
        }))
        .pipe(concat('ct.pre-bundle.js'))
        .pipe(babel({
            presets: ['es2015']
        }))
        // .pipe(uglify({ mangle: false }))
        .pipe(gulp.dest('./dist'));
    }

    // The env name must be defined at build time. Not runtime.
    function getEnvConstants(fs, envName) {
        const envConstants = JSON.parse(fs.readFileSync('./src/app/constants/env.constant.json'));
        return envConstants[envName.toUpperCase()];
    }

    function getLocaleConstants(fs) {
        console.log(JSON.parse(fs.readFileSync('./src/app/constants/pt-br-locale.constant.json')))
        return {
            "PT-BR": JSON.parse(fs.readFileSync('./src/app/constants/pt-br-locale.constant.json')),
            "EN-US": JSON.parse(fs.readFileSync('./src/app/constants/en-us-locale.constant.json'))
        };
    }
}
