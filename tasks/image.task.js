'use strict';

module.exports = (gulp, image) => {
    return () => {
        return gulp.src('./src/assets/imgs/*')
            .pipe(image({
                pngquant: false,
                optipng: false,
                zopflipng: false,
                jpegRecompress: false,
                mozjpeg: true,
                guetzli: false,
                gifsicle: true,
                svgo: true,
                concurrent: 10,
                quiet: true // defaults to false
            }))
            .pipe(gulp.dest('./dist/imgs'))
    }
}
