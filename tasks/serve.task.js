'use strict';

module.exports = (browserSync, connectModrewrite) => {

    const applicationUrls = [
        '/resume',
        '/home'
    ]

    return () => {
        return browserSync.init({
            server: {
                baseDir: './dist'
            },
            middleware: [
                connectModrewrite([
                    '!\\.\\w+$ /index.html [L]'
                ])
            ],
            ghostMode: false
        });
    }
}
