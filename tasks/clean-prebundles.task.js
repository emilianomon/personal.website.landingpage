'use strict';

module.exports = (del) => {
    return () => {
        // return rimraf('./dist', () => 0); // Node was complaining about rimraf not having a callback.
        return del('./dist/*.pre-bundle.js');
    }
}
