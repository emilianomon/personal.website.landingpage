'use strict';

module.exports = (gulp, inject) => {
    return () => {
        return gulp.src('./src/index.html')
            .pipe(gulp.dest('./dist'))
            .pipe(inject(gulp.src(['./dist/main.css',
                                   './dist/bundle.js']), {relative: true}))
            .pipe(gulp.dest('./dist'));
    }
}
