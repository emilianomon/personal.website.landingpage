'use strict';

module.exports = (gulp, concat) => {
    return () => {
        return gulp.src(['./dist/nm.pre-bundle.js', './dist/my.pre-bundle.js', './dist/ct.pre-bundle.js'])
            .pipe(concat('bundle.js'))
            .pipe(gulp.dest('./dist'));
    }
}
