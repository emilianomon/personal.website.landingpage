'use strict';

module.exports = (gulp, embedTemplates, concat, ngAnnotate, babel, uglify) => {

    function getScripts() {
        return [
            './src/app/**/*.js'
        ]
    }

    return () => {
        return gulp.src(getScripts())
            .pipe(embedTemplates())
            .pipe(concat('my.pre-bundle.js'))
            .pipe(ngAnnotate({
                remove: true,
                add: true,
                single_quotes: true
            }))
            .pipe(babel({
                presets: ['es2015']
            }))
            // .pipe(uglify({ mangle: false }))
            .pipe(gulp.dest('./dist'));
    }
}
