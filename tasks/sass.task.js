'use strict';

module.exports = (gulp, sass, csso, postcss, autoprefixer) => {
    return () => {
        return gulp.src('./src/assets/style/main.scss')
            .pipe(sass().on('error', sass.logError))
            .pipe(postcss([autoprefixer({browsers: ['last 2 versions']})]))
            // .pipe(csso()) // Minifies.
            .pipe(gulp.dest('./dist'));
    }
}
