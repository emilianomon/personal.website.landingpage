'use strict';

module.exports = (gulp) => {
    return () => {
        return gulp.src('./src/assets/fonts/*')
            .pipe(gulp.dest('./dist/fonts'));
    }
}
