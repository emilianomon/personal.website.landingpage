'use strict';

module.exports = (gulp, concat) => {

    function getScripts() {
        return [
            // Node Modules
            './node_modules/angular/angular.min.js',
            './node_modules/angular-ui-router/release/angular-ui-router.min.js',
            './node_modules/angular-scroll/angular-scroll.min.js',
            './node_modules/jspdf/dist/jspdf.min.js',
            './node_modules/html2canvas/dist/html2canvas.min.js'
        ]
    }

    return () => {
        return gulp.src(getScripts())
            .pipe(concat('nm.pre-bundle.js'))
            .pipe(gulp.dest('./dist'));
    }
}
