'use strict';
 
module.exports = (gulp) => {
    return () => {
        gulp.src('./src/assets/pdfs/*.pdf')
            .pipe(gulp.dest('./dist/pdfs'));

        return gulp.src('./src/_redirects')
            .pipe(gulp.dest('./dist'));
    }
}
