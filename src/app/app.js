'use strict';

(() => {

    angular
        .module('portfolio.angular', ['ui.router', 'duScroll'])
        .run(run);

    function run($rootScope, LOCALE, localeService) {
        
        console.log('%cHey! Want to take a look at the code?',
                    'color: #393D3F; font-size: 20px;');
        console.log('%cJust click in the Bitbucket icon on the right' +
                    ' upper side of the page or access the following' +
                    ' link: https://bitbucket.org/emilianomon',
                    'color: #546A7B; font-size: 14px;');
        
        setLocale();
        $rootScope.$on('locale-changed', (event, newLocale) => {
            setLocale(newLocale);
        });

        function setLocale(newLocale) {
            $rootScope.LOCALE = localeService.set(newLocale);
        }
    }

})();
