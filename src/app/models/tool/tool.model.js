'use strict';

(() => {

    angular
        .module('portfolio.angular')
        .factory('Tool', Tool);

    function Tool() {
        return function (name, path, link, action) {
            this.name = name;
            this.path = path;
            this.link = link;
            this.action = action;
        }
    }

})();
