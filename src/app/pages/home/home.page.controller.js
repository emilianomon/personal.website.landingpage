'use strict';

(() => {

    angular
        .module('portfolio.angular')
        .controller('homePageController', homePageController);

        
    function homePageController($rootScope, $state, $document, $window, toolsService, httpService) {
        const $ctrl = this;

        $ctrl.$onInit = () => {
            $ctrl.changeLocale = _changeLocale;
            $ctrl.navigateTo = _navigateTo;
            $ctrl.openExternalUrl = _openExternalUrl;
            $ctrl.scrollTo = _scrollTo;

            $ctrl.toolList = toolsService.getAll();
        }

        function _changeLocale(newLocale) {
            $rootScope.$emit('locale-changed', newLocale);
        }

        function _navigateTo($event, state) {
            $state.go(state);
        }

        function _openExternalUrl($event, url, newTab=true) {
            $event.currentTarget.blur();
            $window.open(url, newTab? '_blank' : '_self');
        }

        function _scrollTo($event, elementId, animationPeriod) {
            $event.currentTarget.blur();
            const targetElement = angular.element(document.getElementById(elementId));
            $document.scrollToElementAnimated(targetElement, 0, animationPeriod);
        }
    }
})();
