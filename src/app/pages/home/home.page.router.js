(function() {

    angular
        .module('portfolio.angular')
        .config(homePageRouter);

    
    function homePageRouter($stateProvider) {
        $stateProvider.state('root.home', {
            url: '/',
            templateUrl: './home.page.template.html',
            controller: 'homePageController as $ctrl'
        });

    }

})();
