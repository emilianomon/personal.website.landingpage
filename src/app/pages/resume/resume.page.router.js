'use strict';

(() => {

    angular
        .module('portfolio.angular')
        .config(resumePageRouter);

    function resumePageRouter($stateProvider) {
        $stateProvider.state('root.resume', {
            url: '/resume',
            templateUrl: './resume.page.template.html',
            controller: 'resumePageController as $ctrl'
        })
    }

})();
