'use strict';

(() => {

    angular
        .module('portfolio.angular')
        .controller('resumePageController', resumePageController);

    function resumePageController($rootScope, $state, alertService, $window) {
        const $ctrl = this;

        $ctrl.$onInit = () => {
            const locale = $rootScope.LOCALE.LOCALE_NAME;
            $ctrl.downloadPDF = _downloadPDF;
            $ctrl.navigate = _navigate;
        }

        function _navigate($event) {
            $state.go('root.home');
        }

        function _downloadPDF(language) {
            $window.open('./pdfs/emiliano_oliveira_cv_' + language.substring(0, 2) + '.pdf', '_blank');
        }
    }

})();
