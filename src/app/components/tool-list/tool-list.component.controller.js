'use strict';

(() => {

    angular
        .module('portfolio.angular')
        .component('toolListComponent', {
            bindings: {
                toolList: '='
            },
            controller: toolListComponentController,
            templateUrl: './tool-list.component.template.html'
        });

        
    function toolListComponentController() {
        let $ctrl = this;

        $ctrl.$onInit = () => {
            $ctrl.openToolDetail = _openToolDetail;
        }

        function _openToolDetail(toolObj) {

        }
    }

})();
