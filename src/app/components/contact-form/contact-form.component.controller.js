'use strict';

(() => {

    angular
        .module('portfolio.angular')
        .component('contactFormComponent', {
            bindings: {
                to: '@'
            },
            controller: contactFormComponentController,
            templateUrl: './contact-form.component.template.html'
        });

    function contactFormComponentController($rootScope, alertService, $timeout) {
        let $ctrl = this;

        $ctrl.$onInit = () => {
            $ctrl.emailInfo = {};
            $ctrl.sendMessage = _sendMessage;
        }

        function _sendMessage(event) {
            event.preventDefault();

            const iframe = document.getElementById('tidio-chat-iframe');
            if(iframe) {
                const button = iframe.contentDocument.getElementById('button-body');
                if(button) {
                button.click();
                return;
                }
            }
            
            $timeout(_ => $ctrl.sendMessage(event), 100);

            // alertService.showAlert($rootScope.LOCALE.GENERAL.NOT_IMPLEMENTED,
            //                        $rootScope.LOCALE.GENERAL.CONTACT_THE_FOLLOWING_EMAIL + ': ' + $ctrl.to);
        }
    }
    
})();
