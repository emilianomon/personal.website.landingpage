'use strict';

(() => {

    angular
        .module('portfolio.angular')
        .component('typingComponent', {
            bindings: {
                strPreffix: '@',
                strList: '='
            },
            controller: typingComponentController,
            templateUrl: './typing.component.template.html'
        });
    
        
    function typingComponentController($timeout) {
        let $ctrl = this;

        $ctrl.$onInit = () => {
            
            let typingCounter = 0;
            let stringCounter = 0;
            $ctrl.str = '';

            $timeout(typingEffect, 2000);

            function typingEffect() {
                $timeout(() => {
                    if(typingCounter < $ctrl.strList[stringCounter].length) {
                        $ctrl.str += $ctrl.strList[stringCounter][typingCounter];
                        typingCounter++;
                        typingEffect();
                    } else {
                        stringCounter++;
                        changeString();
                        typingCounter = 0;
                    }
                    
                }, 100)
            }

            function deletingEffect() {
                $timeout(() => {
                    if($ctrl.str.length > 0) {
                        $ctrl.str = $ctrl.str.slice(0, -1);
                        deletingEffect();
                    } else {
                        typingEffect();
                    }
                    
                }, 30)
            }
            
            function changeString() {
                $timeout(() => {
                    if(stringCounter >= $ctrl.strList.length) {
                        stringCounter = 0;
                    }

                    deletingEffect();
                }, 3000);
            }
        }
    }

})();
