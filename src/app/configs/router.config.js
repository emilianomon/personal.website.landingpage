'use strict';

(() => {

    angular
        .module('portfolio.angular')
        .config(routerConfig);

    function routerConfig($locationProvider, $urlRouterProvider, $stateProvider) {

        $stateProvider.state('root', {
            abstract: true,
            url: '',
            views: {
                // 'header': {
                //  template: '<h1>HEADER</h1>'
                // },
                'content': {
                    template: '<ui-view></ui-view>'
                },  
                'footer': {
                    templateUrl: '../partials/footer/footer.partial.html',
                    controller: 'footerPartialController as $ctrl'
                }
            }
        })

        $locationProvider.html5Mode(true);
        $urlRouterProvider.otherwise('/');
    }

})();
