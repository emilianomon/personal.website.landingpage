'use strict';

(() => {

    angular
        .module('portfolio.angular')
        .service('httpService', httpService);

    function httpService(ENV, $http) {

        this.get = _get;
        this.post = _post;

        function _get(url) {
            return $http.get(ENV.BASE_URL + url)
                .then((response) => {
                    return _successCallback(response);
                })
                .catch((error) => {
                    _errorCallback(error);
                });
        }

        function _post(url, params) {
            return $http.post(ENV.BASE_URL + url, params)
                .then((response) => {
                    return _successCallback(response);
                })
                .catch((error) => {
                    _errorCallback(error);
                });
        }

        function _successCallback(response) {
            return response.data;
        }

        function _errorCallback(error) {
            throw error;
        }

    }

})();
