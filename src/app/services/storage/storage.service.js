'use strict';

(() => {

    angular
        .module('portfolio.angular')
        .service('storageService', storageService);

    function storageService() {

        this.get = _get;
        this.set = _set;
        this.clear = _clear;

        function _get(key, useSessionStorage) {
            const storageAccessor = _getStorageAccessor(useSessionStorage);
            return storageAccessor.getItem(key);
        }

        function _set(key, val, useSessionStorage) {
            const storageAccessor = _getStorageAccessor(useSessionStorage);
            return storageAccessor.setItem(key, val);
        }

        function _clear(useSessionStorage) {
            const storageAccessor = _getStorageAccessor(useSessionStorage);
            storageAccessor.clear();
        }

        function _getStorageAccessor(useSessionStorage=false) {
            return useSessionStorage ? sessionStorage : localStorage;
        }
    }

})();
