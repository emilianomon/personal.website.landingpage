'use strict';

(() => {

    angular
        .module('portfolio.angular')
        .service('localeService', localeService);

    function localeService(LOCALE, storageService) {

        const _storageKey = 'locale';
        const _defaultLocale = storageService.get(_storageKey) || 'PT-BR';

        this.set = _set;

        function _set(newLocale=_defaultLocale) {
            try {
                _checkLocale(newLocale);
            }
            catch(error) {
                console.warn('An inalid locale was selected. EN-US was chosen instead.');
                storageService.set(_storageKey, _defaultLocale);
                return LOCALE[_defaultLocale];
            }

            storageService.set(_storageKey, newLocale);
            return LOCALE[newLocale];
        }

        function _checkLocale(newLocale) {
            if(typeof LOCALE[newLocale] == 'undefined') {
                throw new Error('locale unknown');
            }
        }

    }

})();
