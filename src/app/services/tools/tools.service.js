'use strict';

(() => {

    angular
        .module('portfolio.angular')
        .service('toolsService', toolsService);

    function toolsService(Tool) {

        
        this.getAll = () => {
            const response = getFakeTools();

            let toolList = [];
            response.map((tool) => {
                toolList.push(new Tool(
                    tool.name,
                    tool.path,
                    tool.link,
                    null
                ));
            });

            return toolList;
        }

        function getFakeTools() {
            const imgPreffixPath = 'imgs/';
            return [

                // Back-End Frameworks
                {
                    name: 'NodeJs',
                    path: imgPreffixPath + 'nodejs.png',
                    link: null
                },
                {
                    name: 'Dotnet Core',
                    path: imgPreffixPath + 'dotnet.png',
                    link: null
                },

                // WebApp Frameworks
                {
                    name: 'AngularJs/4+',
                    path: imgPreffixPath + 'angular.png',
                    link: 'https://angular.io/'
                },
                {
                    name: 'React',
                    path: imgPreffixPath + 'react.png',
                    link: null
                },

                // Cross Platform Frameworks
                {
                    name: 'React Native',
                    path: imgPreffixPath + 'react-native.png',
                    link: null
                },
                {
                    name: 'Ionic 3',
                    path: imgPreffixPath + 'ionic3.png',
                    link: null
                },
                {
                    name: 'Electron',
                    path: imgPreffixPath + 'electron.png',
                    link: null
                },

                // Task Automation Frameroks
                {
                    name: 'Gulp',
                    path: imgPreffixPath + 'gulp.png',
                    link: null
                },
                {
                    name: 'Webpack',
                    path: imgPreffixPath + 'webpack.png',
                    link: null
                },

                // Programming Languages
                // {
                //     name: 'Typescript',
                //     path: imgPreffixPath + 'typescript.png',
                //     link: null
                // },
                // {
                //     name: 'Ecmascript 8',
                //     path: imgPreffixPath + 'ecmascript8.png',
                //     link: null
                // }
            ];
        }
    }

})();
