'use strict';

(() => {

    angular
        .module('portfolio.angular')
        .service('alertService', alertService);

    function alertService() {

        this.showAlert = _showAlert;

        function _showAlert(title, message) {
            const text = (title ? title : '') + (message ? '\n\n' + message : '')
            alert(text);
        }
    }

})();
