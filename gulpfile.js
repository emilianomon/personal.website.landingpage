'use strict';
 
const autoprefixer = require('autoprefixer');
const babel = require('gulp-babel');
const browserSync = require('browser-sync');
const concat = require('gulp-concat');
const connectModrewrite = require('connect-modrewrite');
const csso = require('gulp-csso');
const del = require('del');
const embedTemplates = require('gulp-angular-embed-templates');
const fs = require('fs');
const gulp = require('gulp');
const image = require('gulp-image');
const inject = require('gulp-inject');
const ngAnnotate = require('gulp-ng-annotate');
const ngConfig = require('gulp-ng-config');
const postcss = require('gulp-postcss');
const runSequence = require('run-sequence');
const sass = require('gulp-sass');
const uglify = require('gulp-uglify');
const watch = require('gulp-watch');

gulp.task('bundle', require('./tasks/bundle.task')(gulp, concat));
gulp.task('clean:prebundles', require('./tasks/clean-prebundles.task')(del));
gulp.task('clean', require('./tasks/clean.task')(del));
gulp.task('constants', require('./tasks/constants.task')(gulp, ngConfig, fs, concat, babel, uglify));
gulp.task('fonts', require('./tasks/fonts.task')(gulp));
gulp.task('html', require('./tasks/html.task')(gulp, inject));
gulp.task('image', require('./tasks/image.task')(gulp, image));
gulp.task('mover', require('./tasks/mover.task')(gulp));
gulp.task('sass', require('./tasks/sass.task')(gulp, sass, csso, postcss, autoprefixer));
gulp.task('scripts:my',require('./tasks/scripts-my.task')(gulp, embedTemplates, concat, ngAnnotate, babel, uglify));
gulp.task('scripts:nm',require('./tasks/scripts-nm.task')(gulp, concat));
gulp.task('serve', require('./tasks/serve.task')(browserSync, connectModrewrite));
 
gulp.task('reload', () => {
    browserSync.reload();
});
 
gulp.task('serve:dev', () => {
    runSequence('clean', 'mover', 'image', 'fonts', 'sass',
                'scripts:nm', 'scripts:my', 'constants',
                'bundle', 'clean:prebundles', 'html', 'serve');
 
    gulp.watch(['./src/**/*.js', './src/**/*.json',
                './src/**/*.scss', './src/assets/**/*.scss', './src/**/*.html'], () => {
        runSequence('clean', 'mover', 'image', 'fonts', 'sass',
                    'scripts:nm', 'scripts:my', 'constants',
                    'bundle', 'clean:prebundles', 'html', 'reload');
    });
});
 
gulp.task('default', () => {
    gulp.start('serve:dev');
});
 
gulp.task('build', () => {
    runSequence('clean','mover', 'image', 'fonts', 'sass',
                'scripts:nm', 'scripts:my', 'constants',
                'bundle', 'clean:prebundles', 'html');
});
